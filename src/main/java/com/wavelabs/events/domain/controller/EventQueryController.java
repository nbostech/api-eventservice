package com.wavelabs.events.domain.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem EventQueryController is a class. getAllEvents() method
 *         retrieves all the events from the uri(i.e from content service).
 * 
 */

@RestController
@Component
@RequestMapping(value = "/event")
public class EventQueryController {
	static final Logger log = Logger.getLogger(EventQueryController.class);
	@Value("${eventUrl}")
	private String eventUrl;
	@Value("${eventIdUrl}")
	private String eventIdUrl;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity getAllEvents() {
		RestTemplate restTemplate = new RestTemplate();
		RestMessage restMessage = new RestMessage();
		ResponseEntity<String> response = restTemplate.getForEntity(eventUrl, String.class);
		if (response != null) {
			return ResponseEntity.status(200).body(response.getBody());
		} else {
			restMessage.message = "404";
			restMessage.messageCode = "Retrieval of Deals Fails!!";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/{eventId}")
	public ResponseEntity getDealById(@PathVariable String eventId) {
		RestTemplate restTemplate = new RestTemplate();
		final String resourceURL = eventIdUrl + eventId;
		RestMessage restMessage = new RestMessage();
		ResponseEntity<String> response = restTemplate.getForEntity(resourceURL, String.class);
		if (response != null) {

			return ResponseEntity.status(200).body(response.getBody());
		} else {
			restMessage.message = "Event not found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

}
